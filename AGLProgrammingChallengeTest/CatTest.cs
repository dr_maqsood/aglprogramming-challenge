﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AGLProgrammingChallenge;
using System.Linq;

namespace AGLProgrammingChallengeTest
{
    [TestClass]
    public class CatTest
    {
        [TestMethod]
        public void TestJsonReader()
        {
            OwnerReader reader = new OwnerReader("http://agl-developer-test.azurewebsites.net/people.json");
            var genderList = reader.GetCatsByGender().GetAwaiter().GetResult();
            Assert.IsNotNull(genderList);            
        }
        [TestMethod]
        public void TestNonCatExists()
        {
            OwnerReader reader = new OwnerReader("http://agl-developer-test.azurewebsites.net/people.json");
            var genderList = reader.GetCatsByGender().GetAwaiter().GetResult();
            var NonCatList = genderList.ToList().Where(x => x.Owner.Any(y => y.Pets != null && y.Pets.Any(z => z.Type != "Cat"))).ToList();
            Assert.IsTrue(NonCatList.Count == 0);
        }
    }
}
