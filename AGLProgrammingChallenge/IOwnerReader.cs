﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLProgrammingChallenge
{
    public interface IOwnerReader
    {
        /// <summary>
        /// Get cats by gender
        /// </summary>
        /// <returns>return list of cats by gender</returns>
        Task<List<OwnerGenderGroup>> GetCatsByGender();
    }
}