﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLProgrammingChallenge
{
    public class OwnerGenderGroup
    {
        public string Gender { get; set; }
        public List<Owner> Owner {get;set;}
    }
}