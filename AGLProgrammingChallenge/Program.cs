﻿using AGLProgrammingChallenge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLProgrammingChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            OwnerReader reader = new OwnerReader(Properties.Settings.Default.WebserviceUrl);
            var genderList = reader.GetCatsByGender().GetAwaiter().GetResult();

            //Display all the cats under a heading of the gender of their owner
            foreach (var gender in genderList)
            {
                //Display Gender
                Console.WriteLine(gender.Gender);
                foreach (var item in gender.Owner.OrderBy(x => x.Name))
                {
                    //Display all cats for this gender
                    Console.WriteLine("\t" + item.Name);
                }
            }
            Console.ReadKey();
        }
    }
}