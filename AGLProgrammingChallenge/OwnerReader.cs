﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AGLProgrammingChallenge
{
    /// <summary>
    /// Owner Reader class
    /// </summary>
    public class OwnerReader
    {
        private string _url;

        public OwnerReader(string url)
        {
            _url = url;
        }
        /// <summary>
        /// Get cats by gender
        /// </summary>
        /// <returns>return list of cats by gender</returns>
       public async Task<List<OwnerGenderGroup>> GetCatsByGender()
       {
            List<OwnerGenderGroup> ownerByGenderList = new List<OwnerGenderGroup>();
            
            using (var client = new HttpClient())
            {
                try
                {
                    using (HttpResponseMessage response = await client.GetAsync(_url))
                    {
                        response.EnsureSuccessStatusCode();

                        if (response.IsSuccessStatusCode)
                        {
                            var jsonString = response.Content.ReadAsStringAsync().Result;
                            var ownerList = JsonConvert.DeserializeObject<List<Owner>>(jsonString);

                            var ownerListByGender = ownerList.ToList()
                            .GroupBy(z => z.Gender)
                            .Select(k => new OwnerGenderGroup
                            {
                                Gender = k.Key,
                                Owner = k.Select(i => new Owner
                                {
                                    Name = i.Name,
                                    Age = i.Age,
                                    Gender = i.Gender,
                                    Pets = i.Pets == null ? null : i.Pets.Where(x => x.Type == "Cat").ToList(),
                                }).ToList()
                            })
                            .ToList();

                            ownerByGenderList = ownerListByGender;
                        }
                    }
                }
                catch(Exception ex)
                {
                    //TODO: write exception in log file
                    throw ex;
                }
                return ownerByGenderList;
            }
        }
    }
}